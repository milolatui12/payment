const { MoleculerError } = require("moleculer").Errors;

const _ = require("lodash");

module.exports = {
	methods: {
		async checkIsAuthenticated(ctx) {
			try {
				const decoded = ctx.meta.auth.credentials;
				const session = await ctx.broker.call(
					"v1.SessionModel.findOne",
					[
						{
							id: decoded.sessionId,
							state: "ACTIVE",
						},
					]
				);
				if (_.get(session, "id", null) === null)
					throw new Error("Unauthenticated");
				const isMatch = ctx.meta.auth.deviceId === session.deviceId;
				if (!isMatch) throw new Error("Unauthenticated");
				ctx.meta.session = decoded.sessionId;
			} catch (error) {
				throw new MoleculerError(
					`[Account] Authenticate: ${error.message}`
				);
			}
		},
		checkUserRole(ctx) {},
		checkOwner(ctx) {},
	},
};
