const Cron = require("moleculer-cron");

module.exports = {
	name: "Cron",
	mixins: [Cron],
	crons: [
		// {
		// 	name: "Random",
		// 	cronTime: "* * * * *",
		// 	onTick: async function () {
		// 		try {
		// 			await this.getLocalService("Cron").broker.call(
		// 				"v1.Order.random"
		// 			);
		// 		} catch (error) {
		// 			console.log(error);
		// 		}
		// 	},
		// 	timeZone: "Asia/Ho_Chi_Minh",
		// },
		// {
			// 	name: "ExpiredOrder",
			// 	cronTime: "5 * * * *",
			// 	onTick: async function () {
			// 		try {
			// 			await this.getLocalService("Cron").broker.call(
			// 				"v1.Order.expiredOrder"
			// 			);
			// 		} catch (error) {
			// 			console.log(error);
			// 		}
			// 	},
			// 	timeZone: "Asia/Ho_Chi_Minh",
			// },
	],
	actions: {},
};
