const gql = require("moleculer-apollo-server").moleculerGql;

module.exports = gql`
	type PaymentQuery {
		Statistics(body: StatisticsInput!): StatisticsResponse
		StatisticsExcel(body: StatisticsInput!): StatisticsExcelResponse
		UserStatistics(body: UserStatisticsInput!): UserStatisticsResponse
		UserStatisticsExcel(body: UserStatisticsInput!): UserStatisticsExcelResponse
	}

	type StatisticsResponse {
		sumary: [Sumary]
		detail: [Detail]
	}
	type StatisticsExcelResponse {
		data: String
	}
	type UserStatisticsResponse {
		sumary: UserStatisticsSumary
		detail: [UserStatisticsDetail]
	}
	type UserStatisticsExcelResponse {
		data: String
	}

	type UserStatisticsSumary {
		totalUser: Int
		failedPayment: Int
		pendingPayment: Int
	}
	type UserStatisticsDetail {
		accountId: Int 
		email: String
		name: String
		payment: [PaymentDetail]
	}

	type PaymentDetail {
		date: String
		totalPayment: Int
		succeededPayment: Int
	}

	type Sumary {
		_id: String
		count: Int
	}

	type Detail {
		_id: String
		total_payment: Int
		succeeded_payment: Int
	}
`;
