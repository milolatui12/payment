const gql = require("moleculer-apollo-server").moleculerGql;

module.exports = gql`
	input StatisticsInput {
		fromDate: String!
		toDate: String!
		method: String
	}

	input UserStatisticsInput {
		fromDate: String!
		toDate: String!
		accountId: Int
	}
`;
