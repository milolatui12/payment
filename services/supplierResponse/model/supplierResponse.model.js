const mongoose = require("mongoose");
const autoIncrement = require("mongoose-auto-increment");

const _ = require("lodash");

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema(
	{
		id: {
			type: Number,
			required: true,
			unique: true,
		},
		transactionId: {
			type: String,
			required: true,
		},
		partnerTransactionId: {
			type: String,
			required: true,
		},
		url: {
			type: String,
			required: true,
		},
		response: {
			type: Object
		},
	},
	{
		collection: "SupplierResponse",
		versionKey: false,
		timestamps: true,
	}
);

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: "id",
	startAt: 1,
	incrementBy: 1,
});

module.exports = mongoose.model(Schema.options.collection, Schema);
