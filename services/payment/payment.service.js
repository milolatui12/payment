const { MoleculerError } = require("moleculer").Errors;

const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});
const { customAlphabet } = require("nanoid");
const { alphanumeric } = require("nanoid-dictionary");
const nanoId = customAlphabet(alphanumeric, 15);

module.exports = {
	name: "Payment",
	version: 1,

	mixins: [QueueMixin],

	settings: {
		amqp: {
			connection: "amqp://127.0.0.1", // You can also override setting from service setting
		},
	},
	actions: {
		test(ctx) {
			console.log(nanoId());
		},
		payWithWallet: {
			params: {
				accountId: "number",
				order: "object",
				transactionId: "string",
			},
			handler: require("./actions/payWithWallet.action"),
		},
		payWithATM: {
			params: {
				accountId: "number",
				order: "object",
				transactionId: "string",
			},
			handler: require("./actions/payWithAtm.action"),
		},
		napas: {
			//listener to napas request
			rest: {
				method: "POST",
				fullPath: "/wh/napas",
				auth: {
					mode: "webhook",
				},
			},
			params: {
				body: {
					$$type: "object",
					code: "string",
					transactionId: "string",
					partnerTransactionId: "string",
				},
			},
			handler: require("./actions/napasWebHook.action"),
		},
		statistics: {
			rest: {
				method: "POST",
				fullPath: "/v1/statistics",
				auth: false
			},
			params: {
				body: {
					$$type: "object",
					fromDate: "string",
					toDate: "string",
					method: "string|optional",
				},
			},
			
			handler: require('./actions/statistics.action'),
		},
		userStatistics: {
			rest: {
				method: "POST",
				fullPath: "/v1/user-statistics",
				auth: false
			},
			params: {
				body: {
					$$type: "object",
					fromDate: "string",
					toDate: "string",
					accountId: "number|optional",
				},
			},
			
			handler: require('./actions/userStatistics.action'),
		},
		excel: {
			rest: {
				method: "GET",
				fullPath: "/v1/statistics-excel",
				auth: false
			},
			params: {
				body: {
					$$type: "object",
					fromDate: "string",
					toDate: "string",
					method: "string|optional",
				},
			},
			handler: require('./actions/statisticsExcel.action')
		},
		userStatisticsExcel: {
			rest: {
				method: "GET",
				fullPath: "/v1/user-statistics-excel",
				auth: false
			},
			params: {
				body: {
					$$type: "object",
					fromDate: "string",
					toDate: "string",
					accountId: "number|optional",
				},
			},
			handler: require('./actions/userStatisticsExcel.action')
		},
	},
	methods: {},
};
