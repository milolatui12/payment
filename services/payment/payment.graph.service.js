const _ = require("lodash");
const AuthMixin = require("../../mixins/authorize.mixin");

const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});

module.exports = {
	name: "Payment.graph",
	mixins: [AuthMixin, QueueMixin],

	version: 1,

	settings: {
		graphql: {
			type: require("../graph/type"),
			input: require("../graph/input"),
			resolvers: {
				PaymentQuery: {
					Statistics: {
						action: "v1.Payment.graph.statistics",
					},
					StatisticsExcel: {
						action: "v1.Payment.graph.statisticsExcel",
					},
					UserStatistics: {
						action: "v1.Payment.graph.userStatistics",
					},
					UserStatisticsExcel: {
						action: "v1.Payment.graph.userStatisticsExcel",
					},
				},
			},
		},
	},

	actions: {
		get: {
			graphql: {
				query: "PaymentQuery: PaymentQuery",
			},
			handler: () => "hello",
		},
		statistics: {
			params: {
				body: {
					$$type: "object",
					fromDate: "string",
					toDate: "string",
					method: "string|optional",
				},
			},

			handler: require("./actions/statistics.action"),
		},
		statisticsExcel: {
			params: {
				body: {
					$$type: "object",
					fromDate: "string",
					toDate: "string",
					method: "string|optional",
				},
			},

			handler: require("./actions/statisticsExcel.action"),
		},
		userStatistics: {
			params: {
				body: {
					$$type: "object",
					fromDate: "string",
					toDate: "string",
					accountId: "number|optional",
				},
			},

			handler: require("./actions/userStatistics.action"),
		},
		userStatisticsExcel: {
			params: {
				body: {
					$$type: "object",
					fromDate: "string",
					toDate: "string",
					accountId: "number|optional",
				},
			},

			handler: require("./actions/userStatisticsExcel.action"),
		},
	},

	created() {},

	async started() {},

	async stopped() {},
};
