const mongoose = require("mongoose");
const autoIncrement = require("mongoose-auto-increment");

const _ = require("lodash");
const PaymentConstant = require('../constants/payment.constant')

autoIncrement.initialize(mongoose);


const Schema = mongoose.Schema(
	{
		id: {
			type: Number,
			required: true,
			unique: true,
		},
        orderId: {
            type: Number,
            required: true
        },
        transactionId: {
            type: String,
            required: true,
            unique: true
        },
        supplierTransaction: {
            type: String | Number, 
            required: false,
        },
        method: {
            type: String,
            enum: _.values(PaymentConstant.METHOD)
        },
        state: {
            type: String,
            enum: _.values(PaymentConstant.STATE)
        },
        supplierResponse: {
            type: Number
        }
	},
	{
		collection: "Payment",
		versionKey: false,
		timestamps: true,
	}
);

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: "id",
	startAt: 1,
	incrementBy: 1,
});

module.exports = mongoose.model(Schema.options.collection, Schema);
