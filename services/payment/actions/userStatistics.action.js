const _ = require("lodash");
const { MoleculerError } = require("moleculer").Errors;

module.exports = async function (ctx) {
	const { fromDate, toDate, accountId } = ctx.params.body;

	try {
		const isValid = Date.parse(toDate) - Date.parse(fromDate) > 0;
		if(!isValid) throw new Error("Invalid date")
		console.time("query_payment_sumary");
		const sumaryQuery = await this.broker.call("v1.PaymentModel.aggregate", [
			[
				accountId
					? {
							$match: {
								"order.accountId": accountId,
								createdAt: {
									$gte: new Date(fromDate),
									$lte: new Date(toDate),
								},
							},
					  }
					: {
							$match: {
								createdAt: {
									$gte: new Date(fromDate),
									$lte: new Date(toDate),
								},
							},
					  },
				{
					$group: {
						_id: "$state",
						count: { $sum: 1 },
					},
				},
			],
		]);

		const paymentStateSumary = sumaryQuery.reduce((a, v) => ({ ...a, [v._id]: v.count }), {});

		console.timeEnd("query_payment_sumary");

		console.time("query_payment_groupby_accountId/date");
		const detailQuery = await this.broker.call("v1.PaymentModel.aggregate", [
			[
				{
					$match: {
						createdAt: {
							$gte: new Date(fromDate),
							$lte: new Date(toDate),
						},
					},
				},
				{
					$lookup: {
						from: "Order",
						localField: "orderId",
						foreignField: "id",
						as: "order",
					},
				},
				{
					$unwind: {
						path: "$order",
					},
				},
				{
					$project: {
						_id: 0,
						method: 1,
						state: 1,
						createdAt: {
							$dateToString: {
								format: "%d-%m-%Y",
								date: "$createdAt",
							},
						},
						accountId: "$order.accountId",
					},
				},
				accountId
					? {
							$match: {
								accountId: accountId,
							},
					  }
					: {
							$match: {},
					  },
				{
					$group: {
						_id: {
							accountId: "$accountId",
							date: "$createdAt",
						},
						state: { $push: "$state" },
					},
				},
				{
					$project: {
						_id: 0,
						accountId: "$_id.accountId",
						date: "$_id.date",
						state: 1,
					},
				},
				{
					$sort: {
						accountId: 1,
						date: 1,
					},
				},
			],
		]);

		console.timeEnd("query_payment_groupby_accountId/date");

		console.time("caculate_succeeded_payment");
		let detail = detailQuery.map((d) => {
			return {
				accountId: d.accountId,
				date: d.date,
				totalPayment: d.state.length,
				succeededPayment: d.state.filter((s) => s === "SUCCEEDED")
					.length,
			};
		});

		detail = _.chain(detail)
			.groupBy("accountId")
			.map((value, key) => ({
				accountId: key,
				payment: value,
			}))
			.value();

		console.timeEnd("caculate_succeeded_payment");

		const emailArr = detail.map((item) => ({ id: item.accountId }));

		console.time("insert_email/name_to_detail");
		const email = await this.broker.call("v1.AccountModel.findMany", [
			accountId ? { id: accountId } : { $or: [...emailArr] },
			{ _id: 0, id: 1, email: 1, firstName: 1 },
		]);

		const emailArrToObj = email.reduce(
			(a, v) => ({ ...a, [v.id]: { email: v.email, name: v.firstName } }),
			{}
		);

		detail.forEach((item) => {
			item.email = emailArrToObj[item.accountId]?.email;
			item.name = emailArrToObj[item.accountId]?.name;
		});
		console.timeEnd("insert_email/name_to_detail");
		return {
			sumary: {
				totalUser: detail.length,
				failedPayment: paymentStateSumary["FAILED"],
				pendingPayment: paymentStateSumary["PENDING"],
			},
			detail: detail,
		};
	} catch (err) {
		throw new MoleculerError(`Statistics: ${err.message}`);
	}
};
