const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const excel = require("excel4node");

const workbook = new excel.Workbook();
const worksheet = workbook.addWorksheet("Sheet 1");

module.exports = async function (ctx) {
	const { fromDate, toDate, method } = ctx.params.body;

	try {
		const isValid = Date.parse(toDate) - Date.parse(fromDate) > 0;
		if(!isValid) throw new Error("Invalid date")
		console.time("sumary_query");
		const sumaryQuery = await this.broker.call("v1.PaymentModel.aggregate", [
			[
				_.isEmpty(method)
					? {
							$match: {
								createdAt: {
									$gte: new Date(fromDate),
									$lte: new Date(toDate),
								},
							},
					  }
					: {
							$match: {
								createdAt: {
									$gte: new Date(fromDate),
									$lte: new Date(toDate),
								},
								method,
							},
					  },
				{
					$group: {
						_id: "$state",
						count: { $sum: 1 },
					},
				},
			],
		]);

		const sumary = sumaryQuery.reduce((a, v) => ({ ...a, [v._id]: v.count }), {});

		console.timeEnd("sumary_query");

		console.timeEnd("detail_query")
		const detail = await this.broker.call("v1.PaymentModel.aggregate", [
			[
				{
					$match: {
						createdAt: {
							$gte: new Date(fromDate),
							$lte: new Date(toDate),
						},
					},
				},
				{
					$project: {
						_id: 0,
						date: {
							$dateToString: {
								format: "%d-%m-%Y",
								date: "$createdAt",
							},
						},
						state: 1
					},
				},
				{
					$group: {
						_id: "$date",
						totalPayment: { $sum: 1 },
						state: { $push: "$state" },
					},
				},
				{
					$project: {
						_id: 0,
						dateString: "$_id",
						date: {
							$dateFromString: {
								dateString: "$_id",
							},
						},
						totalPayment: 1,
						state: 1
					},
				},
				{
					$sort: { date: 1 },
				},
			],
		]);

		detail.forEach(item => {
			item.date = undefined
			item.state = item.state.filter(i => i === "SUCCEEDED").length
		})
		console.timeEnd("detail_query");

		const result = {
			sumary,
			detail,
		};

		const style = workbook.createStyle({
			font: {
				color: "#000000",
				size: 12,
			},
		});

		const titleStyle = workbook.createStyle({
			font: {
				color: "#000000",
				size: 13,
				bold: true,
			},
		});

		worksheet.cell(1, 1).string("sumary").style(style);
		worksheet.cell(2, 1).string(result.sumary[0]._id).style(style);
		worksheet.cell(2, 2).number(result.sumary[0].count).style(style);
		worksheet.cell(3, 1).string(result.sumary[1]._id).style(style);
		worksheet.cell(3, 2).number(result.sumary[1].count).style(style);
		worksheet.cell(4, 1).string(result.sumary[2]._id).style(style);
		worksheet.cell(4, 2).number(result.sumary[2].count).style(style);
		worksheet.cell(5, 1).string("detail").style(style);

		let y = 7;
		//TITLE
		worksheet.cell(y, 1).string("Date").style(titleStyle);
		worksheet.cell(y, 2).string("Total payment").style(titleStyle);
		worksheet.cell(y, 3).string("Succeeded payment").style(titleStyle);
		y++;

		result.detail.forEach((e, i) => {
			worksheet.cell(y, 1).string(e._id).style(style);
			worksheet.cell(y, 2).number(e.totalPayment).style(style);
			worksheet.cell(y, 3).number(e.succeededPayment).style(style);
			y++;
		});
		const filePath = path.join("D:/WORK/payment/assets", "Statictis.xlsx");

		let toBase64 = new Promise((resolve, reject) => {
			workbook.write(filePath, (err, stat) => {
				if (err) {
					reject(err)
				}
				resolve(fs.readFileSync(filePath).toString("base64"));
			});
		});
		// ctx.meta.$responseType = "application/x-www-form-urlencoded";
		// ctx.meta.$responseHeaders = {
		// 	"Content-Disposition": `attachment; filename="Statictis.xlsx"`,
		// };
		// let readStream = fs.createReadStream(`D:/WORK/payment/Excel.xlsx`);
		// return readStream;

		// fs.readFile(filePath, "base64", (err, data) => {
		// 	if (err) {
		// 		console.error(err);
		// 		return;
		// 	}
		// 	console.log(data);
		// });
		// let fileBuffer = fs.readFileSync(filePath).toString("base64");

		return {
			data: await toBase64
		};
	} catch (err) {
		throw new MoleculerError(`Statistics: ${err.message}`);
	}
};
