const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");
const axios = require('axios').default

const baseUrl = "http://localhost:3002/napas";

module.exports = async function (ctx) {
	const { transactionId } = ctx.params;
	try {
		const link = `${baseUrl}/${transactionId}`;

		await this.broker.call("v1.PaymentModel.updateOne", [
			{
				transactionId,
				state: "PENDING",
			},
			{
				state: "REQUIRED_VERIFY",
			},
		]);

		const res = await axios.post(link)

		return {
			code: 1000,
			successed: true,
			message: "Pay order is successful",
			link,
		};
	} catch (err) {
		await this.broker.call("v1.PaymentModel.updateOne", [
			{
				transactionId,
			},
			{
				state: "FAILED",
			},
		]);
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(`Pay with ATM: ${err.message}`);
	}
};
