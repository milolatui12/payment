const _ = require("lodash");

module.exports = async function (ctx) {
	const { fromDate, toDate, method } = ctx.params.body;

	try {
		const isValid = Date.parse(toDate) - Date.parse(fromDate) > 0;
		if(!isValid) throw new Error("Invalid date")
		console.time("sumary_query");
		const sumaryQuery = await this.broker.call("v1.PaymentModel.aggregate", [
			[
				_.isEmpty(method)
					? {
							$match: {
								createdAt: {
									$gte: new Date(fromDate),
									$lte: new Date(toDate),
								},
							},
					  }
					: {
							$match: {
								createdAt: {
									$gte: new Date(fromDate),
									$lte: new Date(toDate),
								},
								method,
							},
					  },
				{
					$group: {
						_id: "$state",
						count: { $sum: 1 },
					},
				},
			],
		]);

		const sumary = sumaryQuery.reduce((a, v) => ({ ...a, [v._id]: v.count }), {});
		console.timeEnd("sumary_query");


		console.time("detail_query")
		const detail = await this.broker.call("v1.PaymentModel.aggregate", [
			[
				{
					$match: {
						createdAt: {
							$gte: new Date(fromDate),
							$lte: new Date(toDate),
						},
					},
				},
				{
					$project: {
						_id: 0,
						date: {
							$dateToString: {
								format: "%d-%m-%Y",
								date: "$createdAt",
							},
						},
						state: 1
					},
				},
				{
					$group: {
						_id: "$date",
						totalPayment: { $sum: 1 },
						state: { $push: "$state" },
					},
				},
				{
					$project: {
						_id: 0,
						dateString: "$_id",
						date: {
							$dateFromString: {
								dateString: "$_id",
							},
						},
						totalPayment: 1,
						state: 1
					},
				},
				{
					$sort: { date: 1 },
				},
			],
		]);

		detail.forEach(item => {
			item.date = undefined
			item.state = item.state.filter(i => i === "SUCCEEDED").length
		})
		console.timeEnd("detail_query");

		return {
			sumary,
			detail,
		};
	} catch (err) {
		throw new MoleculerError(`Statistics: ${err.message}`);
	}
};
