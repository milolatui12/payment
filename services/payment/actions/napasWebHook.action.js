const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const { code, transactionId, partnerTransactionId } = ctx.params.body;

		let payment = {};
		let order = {};
		let supplierRes = {};

		if (code === "SUCCESSED") {
			try {
				supplierRes = await this.broker.call(
					"v1.SupplierResponseModel.create",
					[
						{
							transactionId,
							partnerTransactionId,
							url: `http://localhost:3002/napas/${transactionId}`,
							response: {
								code,
							},
						},
					]
				);
				payment = await this.broker.call("v1.PaymentModel.findOne", [
					{
						transactionId,
					},
				]);
				if (_.get(payment, "id", null) === null) {
					throw new Error("Fail");
				}

				order = await this.broker.call("v1.OrderModel.findOne", [
					{
						id: payment.orderId,
						state: "PENDING",
					},
				]);

				if (_.get(order, "id", null) === null) {
					throw new Error("Fail");
				}

				const orderData = {
					order: {
						...order,
					},
					link: `http://localhost:3002/napas/${transactionId}`,
					transactionId,
					partnerTransactionId,
				};

				await this.broker.call("v1.History.createPayHistory", {
					accountId: order.accountId,
					orderData,
					type: "ATM_CARD",
				});

				order = await this.broker.call(
					"v1.OrderModel.findOneAndUpdate",
					[
						{
							id: order.id,
							state: "PENDING",
						},
						{
							state: "SUCCEEDED",
							payment: {
								...order.payment,
								state: "SUCCEEDED",
							},
						},
						{ new: true },
					]
				);
				if (_.get(order, "id", null) === null) {
					throw new Error("Fail");
				}

				payment = await this.broker.call(
					"v1.PaymentModel.findOneAndUpdate",
					[
						{
							transactionId,
						},
						{
							state: "SUCCEEDED",
							supplierResponse: supplierRes.id,
							supplierTransaction: partnerTransactionId,
						},
						{ new: true },
					]
				);
				if (_.get(payment, "id", null) === null) {
					throw new Error("Fail");
				}
			} catch (error) {
				await this.broker.call("v1.PaymentModel.updateOne", [
					{
						transactionId,
					},
					{
						state: "FAILED",
						supplierResponse: supplierRes.id,
					},
				]);
			}
		}

		return {
			code: 1000,
			data: {
				message: "Successed",
			},
		};
	} catch (error) {
		return {
			code: 1001,
			data: {
				message: error.message,
			},
		};
	}
};
