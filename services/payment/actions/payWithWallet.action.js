const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const { accountId, order, transactionId } = ctx.params;

		let unlock;
		let wallet = {};
		let payment = {};
		try {
			unlock = await this.broker.cacher.lock(`wallet_${accountId}`);

			wallet = await this.broker.call("v1.UserWalletModel.findOne", [
				{ accountId },
			]);

			if (_.get(wallet, "id", null) === null) {
				throw new Error("Fail");
			}

			if (wallet.balance < order.total) {
				throw new Error("Số dư không đủ");
			}

			const newBalance = wallet.balance - order.total;

			wallet = await this.broker.call(
				"v1.UserWalletModel.findOneAndUpdate",
				[
					{
						accountId,
					},
					{
						balance: newBalance,
					},
					{ new: true },
				]
			);

			if (_.get(wallet, "id", null) === null) {
				throw new Error("Fail");
			}

			const orderData = {
				order: {
					...order,
				},
				balanceBefore: wallet.balance,
				balanceAfter: newBalance,
				fromWallet: wallet.id,
				transactionId,
			};

			const history = await this.broker.call("v1.History.createPayHistory", {
				accountId,
				orderData,
				type: "WALLET",
			});

			const newOrderObj = await this.broker.call(
				"v1.OrderModel.findOneAndUpdate",
				[
					{
						id: order.id,
						state: "PENDING",
					},
					{
						state: "SUCCEEDED",
						method: {
							...order.method,
							state: "SUCCESSED"
						}
					},
					{ new: true },
				]
			);
			if (_.get(newOrderObj, "id", null) === null) {
				throw new Error("Fail");
			}

			payment = await this.broker.call(
				"v1.PaymentModel.findOneAndUpdate",
				[
					{
						transactionId,
					},
					{
						state: "SUCCEEDED",
						supplierTransaction: history.id
					},
					{ new: true },
				]
			);

			if (_.get(payment, "id", null) === null) {
				throw new Error("Fail");
			}			
		} catch (err) {
			await this.broker.call("v1.PaymentModel.updateOne", [
				{
					transactionId,
					state: "PENDING",
				},
				{
					state: "FAILED",
				},
			]);
			throw new MoleculerError(err);
		} finally {
			if (_.isFunction(unlock)) {
				await unlock();
			}
		}

		return {
			code: 1000,
			successed: true,
			message: "Pay with wallet successful",
		};
	} catch (err) {
		throw new Error(`${err.message}`);
	}
};
