const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");

const fs = require("fs");
const path = require("path");
const excel = require("excel4node");

const workbook = new excel.Workbook();
const worksheet = workbook.addWorksheet("Sheet 1");

module.exports = async function (ctx) {
	const { fromDate, toDate, accountId } = ctx.params.body;

	try {
		const isValid = Date.parse(toDate) - Date.parse(fromDate) > 0;
		if(!isValid) throw new Error("Invalid date")
		console.time("query_payment_sumary");
		const sumaryQuery = await this.broker.call("v1.PaymentModel.aggregate", [
			[
				accountId
					? {
							$match: {
								"order.accountId": accountId,
								createdAt: {
									$gte: new Date(fromDate),
									$lte: new Date(toDate),
								},
							},
					  }
					: {
							$match: {
								createdAt: {
									$gte: new Date(fromDate),
									$lte: new Date(toDate),
								},
							},
					  },
				{
					$group: {
						_id: "$state",
						count: { $sum: 1 },
					},
				},
			],
		]);

		const paymentStateSumary = sumaryQuery.reduce((a, v) => ({ ...a, [v._id]: v.count }), {});

		const totalOrder = await this.broker.call("v1.OrderModel.aggregate", [
			[
				accountId
					? {
							$match: {
								"order.accountId": accountId,
								createdAt: {
									$gte: new Date(fromDate),
									$lte: new Date(toDate),
								},
							},
					  }
					: {
							$match: {
								createdAt: {
									$gte: new Date(fromDate),
									$lte: new Date(toDate),
								},
							},
					  },
				{
					$group: {
						_id: "$accountId",
					},
				},
				{
					$count: "totalUser",
				},
			],
		]);
		console.timeEnd("query_payment_sumary");

		console.time("query_payment_groupby_accountId/date");
		const detailQuery = await this.broker.call("v1.PaymentModel.aggregate", [
			[
				{
					$match: {
						createdAt: {
							$gte: new Date(fromDate),
							$lte: new Date(toDate),
						},
					},
				},
				{
					$lookup: {
						from: "Order",
						localField: "orderId",
						foreignField: "id",
						as: "order",
					},
				},
				{
					$unwind: {
						path: "$order",
					},
				},
				{
					$project: {
						_id: 0,
						method: 1,
						state: 1,
						createdAt: {
							$dateToString: {
								format: "%d-%m-%Y",
								date: "$createdAt",
							},
						},
						accountId: "$order.accountId",
					},
				},
				accountId
					? {
							$match: {
								"order.accountId": accountId,
							},
					  }
					: {
							$match: {},
					  },
				{
					$group: {
						_id: {
							accountId: "$accountId",
							date: "$createdAt",
						},
						state: { $push: "$state" },
					},
				},
				{
					$project: {
						_id: 0,
						accountId: "$_id.accountId",
						date: "$_id.date",
						state: 1,
					},
				},
				{
					$sort: {
						accountId: 1,
						date: 1,
					},
				},
			],
		]);

		console.timeEnd("query_payment_groupby_accountId/date");

		console.time("caculate_succeeded_payment");
		const detail = detailQuery.map((d) => {
			return {
				accountId: d.accountId,
				date: d.date,
				totalPayment: d.state.length,
				succeededPayment: d.state.filter((s) => s === "SUCCEEDED")
					.length,
			};
		});
		console.timeEnd("caculate_succeeded_payment");

		console.time("insert_email/name_to_detail");
		const email = await this.broker.call("v1.AccountModel.findMany", [
			{},
			{ _id: 0, id: 1, email: 1, firstName: 1 },
		]);
		const emailArrToObj = email.reduce(
			(a, v) => ({ ...a, [v.id]: { email: v.email, name: v.firstName } }),
			{}
		);

		detail.forEach((item) => {
			item.email = emailArrToObj[item.accountId]?.email;
			item.name = emailArrToObj[item.accountId]?.name;
		});
		console.timeEnd("insert_email/name_to_detail");

		const result = {
			sumary: {
				totalUser: totalOrder[0].totalUser,
				failedPayment: paymentStateSumary["FAILED"],
				pendingPayment: paymentStateSumary["PENDING"],
			},
			detail: detail,
		};

		const style = workbook.createStyle({
			font: {
				color: "#000000",
				size: 12,
			},
		});
		const titleStyle = workbook.createStyle({
			font: {
				color: "#000000",
				size: 13,
				bold: true,
			},
		});

		worksheet.cell(1, 1).string("SUMARY").style(style);
		worksheet.cell(2, 1).string("Total user").style(style);
		worksheet.cell(2, 2).number(result.sumary.totalUser).style(style);
		worksheet.cell(3, 1).string("Payment failed").style(style);
		worksheet.cell(3, 2).number(result.sumary.failedPayment).style(style);
		worksheet.cell(4, 1).string("Payment pending").style(style);
		worksheet.cell(4, 2).number(result.sumary.pendingPayment).style(style);

		worksheet.cell(6, 1).string("DETAIL").style(style);

		let y = 7;
		//TITLE
		worksheet.cell(y, 1).string("Account ID").style(titleStyle);
		worksheet.cell(y, 2).string("Name").style(titleStyle);
		worksheet.cell(y, 3).string("Email").style(titleStyle);
		worksheet.cell(y, 4).string("Date").style(titleStyle);
		worksheet.cell(y, 5).string("Total payment").style(titleStyle);
		worksheet.cell(y, 6).string("Succeeded payment").style(titleStyle);
		y++;

		result.detail.forEach((e) => {
			worksheet.cell(y, 1).number(e.accountId).style(style);
			worksheet.cell(y, 2).string(e.name).style(style);
			worksheet.cell(y, 3).string(e.email).style(style);
			worksheet.cell(y, 4).string(e.date).style(style);
			worksheet.cell(y, 5).number(e.totalPayment).style(style);
			worksheet.cell(y, 6).number(e.succeededPayment).style(style);
			y++;
		});

		const filePath = path.join("D:/WORK/payment/assets", "UserStatictis.xlsx");

		let toBase64 = new Promise((resolve, reject) => {
			workbook.write(filePath, (err, stat) => {
				if (err) {
					reject(err);
				}
				resolve(fs.readFileSync(filePath).toString("base64"));
			});
		});
		return { data: await toBase64 };
	} catch (err) {
		throw new MoleculerError(`Statistics: ${err.message}`);
	}
};
