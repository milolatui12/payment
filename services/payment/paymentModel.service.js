const DbService = require("moleculer-db");
const MongooseAdapter = require("moleculer-db-adapter-mongoose");
const MongooseAction = require("moleculer-db-adapter-mongoose-action");

const PaymentModel = require('./model/payment.model')

module.exports = {
	name: "PaymentModel",
	version: 1,
	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true,
	}),

	model: PaymentModel,

	settings: {},
	actions: MongooseAction(),
	methods: {},
	events: {},
	created() {},
	async started() {},
	async stopped() {},
	async afterConnected() {
		this.logger.info("Connected successfully...");
	},
	dependencies: [],
	// metadata: {
	// 	scalable: true,
	// 	priority: 5,
	// },
};
